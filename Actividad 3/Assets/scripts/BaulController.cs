using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaulController : MonoBehaviour
{
    Animator _baulAnim;

    private void OnTriggerEnter(Collider other){
        _baulAnim.SetBool("isOpeningBaul", true);
    }

    private void OnTriggerExit(Collider other){
        _baulAnim.SetBool("isOpeningBaul", false);

    }

    void Start(){
        _baulAnim = this.transform.parent.GetComponent<Animator>();
    }
}
