using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    public float rotationUnits;
    void Update()
    {
        axes= CapsuleMovement.ClampVector3(axes);
        transform.Rotate(rotationUnits * (axes * Time.deltaTime));
    }
}

